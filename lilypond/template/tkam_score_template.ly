%\version "2.19.83"
\version "2.24.1"

#(define (override-color-for-all-grobs color)
  (lambda (context)
   (let loop ((x all-grob-descriptions))
    (if (not (null? x))
     (let ((grob-name (caar x)))
      (ly:context-pushpop-property context grob-name 'color color)
      (loop (cdr x)))))))

 #(define-markup-command (relMark layout props mus) (ly:music?)
    #:properties ((size -2))
    (interpret-markup layout props
      #{
        \markup {
          \score {
            \new Staff { $mus }
            \layout {
              \context {
                \Staff
                \remove Time_signature_engraver
                fontSize = #-2
                \hide Stem
                \override TextScript.outside-staff-priority = ##f
                \override StaffSymbol.staff-space = #(magstep -2)
                \override StaffSymbol.thickness = #(magstep -2)
                \override TextScript.self-alignment-X = #-0.4
                \override TextScript.staff-padding = #1
              }
              \context {
                \Score
                proportionalNotationDuration = #(ly:make-moment 1/16)
                \remove "Separating_line_group_engraver"
                \override SpacingSpanner.strict-note-spacing = ##t
                \override RehearsalMark.self-alignment-X = #-1
                \override RehearsalMark.Y-offset = #10
                \override RehearsalMark.X-offset = #10
              }
              \context {
                \Voice
                \consists "Horizontal_bracket_engraver"
                \override HorizontalBracket.direction = #UP
              }
              indent = 0
              line-width = 4\cm
            }
          }
        }
      #}))
 

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 2 \cm
  ragged-bottom = ##t
  
  top-system-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  system-system-spacing = 
  #'((basic-distance . 35 )
  (minimum-distance . 35 )
  (padding . 0 )
  (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 10 )
  (minimum-distance . 10 )
  (padding . 0 )
  (stretchability . 0))
  
  %systems-per-page = 3
  first-page-number = 1
  print-first-page-number = ##t
  
  % for lilypond version 2.19.83
  %print-page-number = ##t
  %oddHeaderMarkup = \markup { \fill-line { \line { \on-the-fly #not-first-page {\pad-markup #2 { \concat {\italic {"to kill a monarch "} (seed: xxx)}}}}}}
  %evenHeaderMarkup = \markup { \fill-line { \line { \on-the-fly #not-first-page {\pad-markup #2 { \concat {\italic {"to kill a monarch "} (seed: xxx)}}}}}}
  %oddFooterMarkup = \markup { \fill-line {
  %  \concat {
  %    "-"
  %    \fontsize #1.5
  %    \on-the-fly #print-page-number-check-first
  %    \fromproperty #'page:page-number-string
  %    "-"}}}
  %evenFooterMarkup = \markup { \fill-line {
  %  \concat { 
  %    "-" 
  %    \fontsize #1.5
  %    \on-the-fly #print-page-number-check-first
  %    \fromproperty #'page:page-number-string
  %    "-"}}}
%}

  % for lilypond version 2.24.1
  print-page-number = ##t
  oddHeaderMarkup = \markup { \fill-line { \line { \unless \on-first-page {\pad-markup #2 { \concat {\italic {"to kill a monarch "} (seed: xxx)}}}}}}
  evenHeaderMarkup = \markup { \fill-line { \line { \unless \on-first-page {\pad-markup #2 { \concat {\italic {"to kill a monarch "} (seed: xxx)}}}}}}
  oddFooterMarkup = \markup { \fill-line {
    \concat {
      "-"
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \concat { 
      "-" 
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
}

\header {
  title = \markup { \italic {to kill a monarch}}
  composer = \markup \right-column {"michael winter" "(berlin, germany; 2021)"}
  poet = "seed: 19800725"
  tagline = ""
} 

\header {
  title = \markup { \italic {to kill a monarch}}
  composer = \markup \right-column {"michael winter" "(berlin, germany; 2021)"}
  poet = "seed: xxx"
  tagline = ""
} 

#(set-global-staff-size 11)

\layout {
  indent = 0.0\cm
  line-width = 17.5\cm 
  ragged-last = ##f
  ragged-right = ##f
  
  \context {
    \Score
      \override BarNumber.stencil = #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
      \override Stem.stemlet-length = #0.75
      proportionalNotationDuration = #(ly:make-moment 1/16)
      \remove "Separating_line_group_engraver"
      \override RehearsalMark.self-alignment-X = #-1 
      \override RehearsalMark.Y-offset = #10
      \override RehearsalMark.X-offset = #-8
      %\override RehearsalMark.outside-staff-priority = #0
      %added for new lilypond
      rehearsalMarkFormatter = #format-mark-box-numbers
  }
  \context {
    \Staff
    
    \override VerticalAxisGroup.staff-staff-spacing =
      #'((basic-distance . 20 )
      (minimum-distance . 20 )
      (padding . 0 )
      (stretchability . 0))

      \override VerticalAxisGroup.default-staff-staff-spacing =
      #'((basic-distance . 20 )
      (minimum-distance . 20 )
      (padding . 0 )
      (stretchability . 0))
      \override TextScript.staff-padding = #2
      \override TextScript.self-alignment-X = #0
  }
  \context {
    \StaffGroup
    \name "SemiStaffGroup"
    \consists "Span_bar_engraver"
    \override SpanBar.stencil =
      #(lambda (grob) 
        (if (string=? (ly:grob-property grob 'glyph-name) "|")
            (set! (ly:grob-property grob 'glyph-name) ""))
        (ly:span-bar::print grob))
  }
  \context {
    \Score
    \accepts SemiStaffGroup
  }
}

\midi { }


\score{
\new Score 
  <<
    \new SemiStaffGroup {
    <<
    \include "includes/part_I.ly"
    \include "includes/part_II.ly"
    \include "includes/part_III.ly"
    >>
    }
    \include "includes/part_star.ly"
  >>
  
  \layout{}
  \midi{}
}
