\new StaffGroup  \with {\remove "System_start_delimiter_engraver"}
<<
\new Staff = "III" \with { 
instrumentName = "III" 
shortInstrumentName = "III" 
midiInstrument = #"clarinet" 

}
<<

{  \tempo 2 = 60
 \numericTimeSignature \time 2/2
 \clef bass
  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.1 } 
\stopStaff s4. \startStaff \clef bass s16 
e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { r1  }
>>
 |  
<<
 { r1  }
>>
 |  
<<
 { r1  }
>>
 |  
<<
 { r1  }
>>
 | \break \noPageBreak 
<<
 { r1  }
>>
 |  
<<
 { r4 r16[ ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } fis,8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,2 ~ }
>>
 |  
<<
 { fis,16[ b,8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,2. ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.2 } 
 { b,1 ~ }
>>
 | \break \noPageBreak 
<<
 { b,4 r4 d4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ d8[ g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { g,1 }
>>
 |  
<<
 { fis,4^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ fis,8[ b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,2 ~ }
>>
 |  
<<
 { e,1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.3 } 
 { e,1 ~ }
>>
 |  
<<
 { e,2 ~ e,8[ r8] r8[ g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] }
>>
 |  
<<
 { d8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ e,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,16[ b,8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,4 ~ }
>>
 |  
<<
 { b,4 ~ b,16[ fis,8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,2 ~ }
>>
 | \break \noPageBreak 
<<
 { fis,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.4 } 
 { fis,1 ~ }
>>
 |  
<<
 { fis,2. ~ fis,8.[ r16] }
>>
 |  
<<
 { r8.[ ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,2. ~ }
>>
 | \break \noPageBreak 
<<
 { ais,4 ~ ais,16[ d8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] c4^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } fis,8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b,8.[ g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,2. ~ }
>>
 |  
<<
 { g,2 ~ g,8.[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,4 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.5 } 
 { e,1 ~ }
>>
 | \break \noPageBreak 
<<
 { e,8.[ r16] r8.[ ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,16[ e,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,4 ~ }
>>
 |  
<<
 { e,2. ~ e,8[ d8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 | \break \noPageBreak 
<<
 { d1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.6 } 
 { d1 ~ }
>>
 |  
<<
 { d2. ~ d8[ r8] }
>>
 |  
<<
 { r8[ c8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c16[ e,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,2 ~ }
>>
 | \pageBreak 
<<
 { e,2 b,8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,4 ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.7 } 
 { g,1 ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 | \break \noPageBreak 
<<
 { g,1 ~ }
>>
 |  
<<
 { g,2 ~ g,16[ r8.] r16[ b,8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b,8[ e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,2. ~ }
>>
 |  
<<
 { e,8[ ais,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,2. ~ }
>>
 | \break \noPageBreak 
<<
 { ais,4 d2.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.8 } 
 { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 | \break \noPageBreak 
<<
 { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 |  
<<
 { d16[ r8.] r16[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c4 ~ c8.[ g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.9 } 
 { g,1 ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 |  
<<
 { g,4 ~ g,16[ r8.] r16[ e,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,4 ~ }
>>
 |  
<<
 { e,1 ~ }
>>
 | \break \noPageBreak 
<<
 { e,4 ~ e,8.[ ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,4 ~ ais,8[ fis,8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis,2 d2^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.10 } 
 { d1 ~ }
>>
 |  
<<
 { d2 r4 e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 | \break \noPageBreak 
<<
 { b,8[ d8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d16[ g,8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,8[ c8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c4 ~ }
>>
 |  
<<
 { c8[ d8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ais,8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,4 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.11 } 
 { b,1 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 | \break \noPageBreak 
<<
 { b,8[ r8] r8[ d8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] fis,8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.12 } 
 { e,1 ~ }
>>
 |  
<<
 { e,1 }
>>
 | \pageBreak 
<<
 { r4 g,2.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 |  
<<
 { g,16[ d8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] c4^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ c16[ fis,8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,4 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.13 } 
 { fis,1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,2 ~ fis,16[ r8.] r16[ b,8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b,2. ~ b,8.[ g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 | \break \noPageBreak 
<<
 { g,1 ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.14 } 
 { g,1 ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 | \break \noPageBreak 
<<
 { g,1 ~ }
>>
 |  
<<
 { g,4 ~ g,8.[ r16] r8.[ b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,4 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.15 } 
 { b,2. ~ b,8[ r8] }
>>
 | \break \noPageBreak 
<<
 { r8[ c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,2 e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ c8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d4 ~ d16[ fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,16[ fis,8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis,2. ais,4^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { ais,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.16 } 
 { ais,1 ~ }
>>
 |  
<<
 { ais,1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais,2 ~ ais,16[ r8.] r16[ e,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] }
>>
 |  
<<
 { c1^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c2. ~ c16[ e,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.17 } 
 { e,1 ~ }
>>
 |  
<<
 { e,2 ~ e,8.[ r16] r8.[ d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] }
>>
 |  
<<
 { g,1^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 |  
<<
 { g,8[ c8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c4 e,2^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 | \pageBreak 
<<
 { e,1 ~ }
>>
 |  
<<
 { e,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.18 } 
 { e,1 ~ }
>>
 |  
<<
 { e,1 ~ }
>>
 | \break \noPageBreak 
<<
 { e,4 ~ e,16[ r8.] r16[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,4 ~ }
>>
 |  
<<
 { ais,16[ fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } c8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c2 ~ c8.[ d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d4 ~ d8[ e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,2 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.19 } 
 { e,1 ~ }
>>
 | \break \noPageBreak 
<<
 { e,1 ~ }
>>
 |  
<<
 { e,1 ~ }
>>
 |  
<<
 { e,2. ~ e,16[ r8.] }
>>
 |  
<<
 { r16[ g,8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,2 ais,8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { fis,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.20 } 
 { fis,1 ~ }
>>
 |  
<<
 { fis,16[ r8.] r16[ b,8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,2 ~ }
>>
 |  
<<
 { b,2. ais,4^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 | \break \noPageBreak 
<<
 { ais,1 ~ }
>>
 |  
<<
 { ais,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.21 } 
 { ais,1 }
>>
 |  
<<
 { r4 c2^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } e,4^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 | \break \noPageBreak 
<<
 { e,1 ~ }
>>
 |  
<<
 { e,2 d2^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { d2 g,2^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.22 } 
 { g,1 ~ }
>>
 | \break \noPageBreak 
<<
 { g,1 ~ }
>>
 |  
<<
 { g,8[ r8] r8[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,4 ~ b,8.[ ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { ais,4 e,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,8.[ g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 | \break \noPageBreak 
<<
 { g,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.23 } 
 { g,1 ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 | \pageBreak 
<<
 { g,4 ~ g,8.[ r16] r8.[ fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,4 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,16[ c8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ ais,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,2 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.24 } 
 { ais,1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais,1 ~ }
>>
 |  
<<
 { ais,16[ r8.] r16[ d8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d2 ~ }
>>
 |  
<<
 { d2 ~ d8[ g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c16[ e,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e,16[ b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,8[ d8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d2 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.25 } 
 { d1 ~ }
>>
 |  
<<
 { d4 ~ d16[ r8.] r16[ c8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c16[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e,2. ~ e,8.[ g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 | \break \noPageBreak 
<<
 { g,1 }
>>
 |  
<<
 { d1^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.26 } 
 { d1 ~ }
>>
 |  
<<
 { d2 ~ d8.[ r16] r8.[ fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { fis,2. ~ fis,16[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b,8.[ fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,4 e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b,16[ c8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c2. ~ }
>>
 |  
<<
 { c1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.27 } 
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c2 ~ c8.[ r16] r8.[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 | \break \noPageBreak 
<<
 { e,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.28 } 
 { e,1 ~ }
>>
 |  
<<
 { e,2. ~ e,16[ r8.] }
>>
 |  
<<
 { r16[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] e,4^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ e,8.[ d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d4 ~ }
>>
 | \break \noPageBreak 
<<
 { d2 g,8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,4 }
>>
 |  
<<
 { e,1^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.29 } 
 { e,1 ~ }
>>
 |  
<<
 { e,1 ~ }
>>
 | \pageBreak 
<<
 { e,1 ~ }
>>
 |  
<<
 { e,4 r4 c2^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } }
>>
 |  
<<
 { fis,1^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.30 } 
 { fis,2. ~ fis,8[ r8] }
>>
 |  
<<
 { r8[ g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,2. ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 | \break \noPageBreak 
<<
 { g,1 ~ }
>>
 |  
<<
 { g,8[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,2. ~ }
>>
 |  
<<
 { b,8.[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ c8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c8.[ ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] g,4^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.31 } 
 { g,1 ~ }
>>
 | \break \noPageBreak 
<<
 { g,1 ~ }
>>
 |  
<<
 { g,8[ r8] r8[ e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,8.[ b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ ais,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e,8[ c8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c16[ fis,8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ais,8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,8[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b,2 e,2^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.32 } 
 { e,2. ~ e,8.[ r16] }
>>
 |  
<<
 { r8.[ d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d2. ~ }
>>
 |  
<<
 { d4 ~ d8[ g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,2 ~ }
>>
 |  
<<
 { g,8[ c8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c2. ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.33 } 
 { c2. ~ c8.[ r16] }
>>
 |  
<<
 { r8.[ ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,16[ fis,8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,8.[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,4 ~ }
>>
 |  
<<
 { e,1 ~ }
>>
 |  
<<
 { e,1 ~ }
>>
 | \break \noPageBreak 
<<
 { e,2. ~ e,16[ ais,8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.34 } 
 { ais,1 ~ }
>>
 |  
<<
 { ais,1 ~ }
>>
 |  
<<
 { ais,1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais,1 ~ }
>>
 |  
<<
 { ais,4 ~ ais,16[ r8.] r16[ d8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d4 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.35 } 
 { d1 ~ }
>>
 | \pageBreak 
<<
 { d4 ~ d16[ r8.] r16[ b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,16[ ais,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d8.[ fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,4 b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ fis,8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis,8[ g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c4 ~ c8[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,4 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.36 } 
 { b,1 ~ }
>>
 | \break \noPageBreak 
<<
 { b,8[ r8] r8[ d8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d2 ~ }
>>
 |  
<<
 { d8[ g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,4 ~ g,16[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } fis,8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,4 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.37 } 
 { fis,1 ~ }
>>
 |  
<<
 { fis,2. ~ fis,8[ r8] }
>>
 |  
<<
 { r8[ ais,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,2 ~ ais,8.[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] }
>>
 | \break \noPageBreak 
<<
 { d4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ d16[ b,8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,4 ~ b,16[ ais,8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { ais,16[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,2. ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.38 } 
 { b,1 ~ }
>>
 |  
<<
 { b,2 ~ b,8[ r8] r8[ g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 | \break \noPageBreak 
<<
 { g,4 ~ g,8[ c16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] e,4^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ e,16[ ais,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,4 g,2.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 | \break \noPageBreak 
<<
 { g,2. ~ g,8[ b,8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.39 } 
 { b,1 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 |  
<<
 { b,2. ~ b,8[ r8] }
>>
 | \break \noPageBreak 
<<
 { r8[ fis,8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,2. ~ }
>>
 |  
<<
 { fis,2 ~ fis,8.[ d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d4 ~ }
>>
 |  
<<
 { d8.[ e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,16[ ais,8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,2 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.40 } 
 { ais,1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais,1 ~ }
>>
 |  
<<
 { ais,16[ r8.] r16[ c8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c4 ~ c16[ e,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e,16[ b,8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,2. ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 | \pageBreak 
<<
 { b,1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.41 } 
 { b,1 ~ }
>>
 |  
<<
 { b,1 }
>>
 |  
<<
 { r4 d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ c8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,16[ fis,8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,4 ~ }
>>
 | \break \noPageBreak 
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,2 d8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,4 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.42 } 
 { b,1 ~ }
>>
 | \break \noPageBreak 
<<
 { b,2 ~ b,16[ r8.] r16[ g,8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 |  
<<
 { g,2 ~ g,16[ ais,8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,4 ~ }
>>
 \bar ".|"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.43 } 
 { ais,1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais,4 r4 b,4^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ b,8[ e,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e,8[ c8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c16[ d16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } g,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,8[ ais,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] e,4^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { e,16[ fis,8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,8[ g,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g,2 ~ }
>>
 |  
<<
 { g,1 ~ }
>>
 | \break \noPageBreak 
<<
 { g,1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+0"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+0"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { g,1 ~ }
>>
 |  
<<
 { g,4 r4 e2^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { e8.[ c16^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ c8.[ d16^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ d2 ~ }
>>
 | \break \noPageBreak 
<<
 { d1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.2 } 
\stopStaff s4. \startStaff \clef bass s16 
fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+0"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+2"} \hide c''8 
 e'16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 ~ }
>>
 |  
<<
 { d2. ~ d8[ r8] }
>>
 |  
<<
 { r8[ fis,8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ fis,8[ gis,16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } cis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ cis4 ~ cis8.[ e16^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { e4 ~ e8.[ cis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ cis2 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "+4"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+2"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+0"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 | \break \noPageBreak 
<<
 { cis4 ~ cis16[ r8.] r16[ d8.^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ d16[ cis8.^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis8.[ g16^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ g2. ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.4 } 
\stopStaff s4. \startStaff \clef bass s16 
b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+4"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { g2. ~ g8[ r8] }
>>
 | \pageBreak 
<<
 { r8[ a8^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ a2. ~ }
>>
 |  
<<
 { a2 fis2^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { fis2. b,4^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 | \break \noPageBreak 
<<
 { b,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+2"} \hide c8 
 e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+0"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+2"} \hide c''8 
 ais'16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b,1 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 | \break \noPageBreak 
<<
 { b,16[ r8.] r16[ ais,8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,2 ~ }
>>
 |  
<<
 { ais,2 ~ ais,8.[ b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,4 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+0"} \hide c8 
 ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+2"} \hide c'8 
 ais16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
ais'16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b,1 ~ }
>>
 | \break \noPageBreak 
<<
 { b,8.[ r16] r8.[ bes,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ bes,4 ~ bes,8.[ des16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { des16[ c16^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } bes,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ bes,4 ~ bes,16[ f8.^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ f16[ g8^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } des16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { des16[ f8.^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ f2 ~ f16[ g8.^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { g16[ bes,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ees8^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ees2. ~ }
>>
 | \break \noPageBreak 
<<
 { ees1 ~ }
>>
 |  
<<
 { ees4 g2.^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { g2. ~ g16[ c8.^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { c1 ~ }
>>
 \bar "|.|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.1 } 
\stopStaff s4. \startStaff \clef bass s16 
ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais16^\markup { "-49"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais'16^\markup { "-49"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c8[ r8] r8[ des8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ des8.[ f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ f4 ~ }
>>
 | \break \noPageBreak 
<<
 { f8[ ges8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] g2.^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { g1 ~ }
>>
 |  
<<
 { g1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais,16^\markup { "-49"} \hide c8 
 f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
fis16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { g1 ~ }
>>
 | \break \noPageBreak 
<<
 { g2. ~ g8.[ r16] }
>>
 |  
<<
 { r8.[ f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f,2. ~ }
>>
 |  
<<
 { f,8[ d8^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d2. ~ }
>>
 |  
<<
 { d1 ~ }
>>
 | \pageBreak 
<<
 { d1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.3 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "-8"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-47"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 ~ }
>>
 |  
<<
 { d2. ~ d16[ r8.] }
>>
 |  
<<
 { r16[ bes,8.^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ bes,2. ~ }
>>
 | \break \noPageBreak 
<<
 { bes,2. ~ bes,8.[ des16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 |  
<<
 { des16[ aes,8^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } g,16^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] c2.^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.4 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-47"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "-8"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 | \break \noPageBreak 
<<
 { c2 ~ c8.[ r16] r8.[ d16^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d8[ f,8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f,2. ~ }
>>
 |  
<<
 { f,1 ~ }
>>
 |  
<<
 { f,1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.5 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "-8"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-47"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f,1 ~ }
>>
 |  
<<
 { f,1 }
>>
 |  
<<
 { r4 d8^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ f,8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f,2 ~ }
>>
 |  
<<
 { f,1 ~ }
>>
 | \break \noPageBreak 
<<
 { f,16[ aes,8.^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ aes,2. ~ }
>>
 |  
<<
 { aes,1 ~ }
>>
 |  
<<
 { aes,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f,16^\markup { "-47"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "-8"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { aes,1 ~ }
>>
 | \break \noPageBreak 
<<
 { aes,1 ~ }
>>
 |  
<<
 { aes,2 r4 d4^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { d4 des2.^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { des1 ~ }
>>
 \bar ".|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.7 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-8"} \hide c8 
 f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { des1 ~ }
>>
 |  
<<
 { des1 ~ }
>>
 |  
<<
 { des1 ~ }
>>
 |  
<<
 { des1 ~ }
>>
 | \break \noPageBreak 
<<
 { des1 ~ }
>>
 |  
<<
 { des8.[ r16] r8.[ g,16^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ g,8.[ d16^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d16[ aes,8.^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 |  
<<
 { aes,4 ~ aes,8.[ f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f,16[ g,8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } des16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ des16[ bes,8.^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { bes,1 ~ }
>>
 | \pageBreak 
<<
 { bes,1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.1 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-47"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes,1 ~ }
>>
 |  
<<
 { bes,2. ~ bes,8[ r8] }
>>
 |  
<<
 { r8[ d8^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d2. ~ }
>>
 | \break \noPageBreak 
<<
 { d1 ~ }
>>
 |  
<<
 { d2 ~ d16[ des8.^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ des4 ~ }
>>
 |  
<<
 { des4 bes,2.^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f,16^\markup { "-47"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+40"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes,1 ~ }
>>
 | \break \noPageBreak 
<<
 { bes,1 ~ }
>>
 |  
<<
 { bes,2 ~ bes,16[ r8.] r16[ c16^\markup { \pad-markup #0.2 "+26"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ees8^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { ees16[ aes,8.^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes,8.[ f16^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] bes,2^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 |  
<<
 { bes,8.[ ges16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ees4^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ ees8.[ ges16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ ges4 ~ }
>>
 | \break \noPageBreak 
<<
 { ges1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.3 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-6"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-47"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ges1 ~ }
>>
 |  
<<
 { ges1 ~ }
>>
 |  
<<
 { ges1 ~ }
>>
 | \break \noPageBreak 
<<
 { ges1 ~ }
>>
 |  
<<
 { ges4 ~ ges16[ r8.] r16[ c8.^\markup { \pad-markup #0.2 "+26"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ c8.[ aes,16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { aes,4 ~ aes,8.[ ges16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ ges2 ~ }
>>
 |  
<<
 { ges1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.4 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-47"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "-6"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ges1 ~ }
>>
 |  
<<
 { ges2. r4 }
>>
 |  
<<
 { f2^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ees2^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { ees4 ges2.^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 | \break \noPageBreak 
<<
 { ges1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+40"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
cis'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-47"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ges1 ~ }
>>
 |  
<<
 { ges4 ~ ges16[ r8.] r16[ cis8.^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ cis4 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 | \break \noPageBreak 
<<
 { cis4 ~ cis8.[ fis16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ fis4 dis8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ gis8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 |  
<<
 { gis2 b2^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 | \pageBreak 
<<
 { b1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 5.1 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/7" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-6"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "-6"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b2 ~ b16[ r8.] r16[ e8.^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { e1 }
>>
 |  
<<
 { cis4^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ cis8[ e8^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ e4 ~ e16[ fis8.^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 5.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "-6"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/7" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "+45"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "-37"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis1 ~ }
>>
 |  
<<
 { fis1 }
>>
 |  
<<
 { r4 gis2^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } fis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ b,8.^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { b,16[ g8.^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ g8.[ e16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] cis8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ d16^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } gis16^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ gis8[ b,8^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 | \break \noPageBreak 
<<
 { b,1 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 5.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "-37"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/7" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/7" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-6"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+45"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b,1 ~ }
>>
 |  
<<
 { b,2 r4 dis4^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 | \break \noPageBreak 
<<
 { dis8.[ b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b2. ~ }
>>
 |  
<<
 { b16[ gis8.^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ gis4 ~ gis8[ b8^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b4 ~ }
>>
 |  
<<
 { b4 dis2^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ dis8.[ gis16^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 \bar ".|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 5.4 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "-6"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { gis1 ~ }
>>
 |  
<<
 { gis16[ r8.] r16[ g8.^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ g2 ~ }
>>
 |  
<<
 { g4 ~ g8.[ b,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b,16[ cis8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } d16^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ d4 ~ }
>>
 |  
<<
 { d8.[ b,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b,2. ~ }
>>
 | \break \noPageBreak 
<<
 { b,1 ~ }
>>
 |  
<<
 { b,16[ gis8.^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ gis2. ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 | \break \noPageBreak 
<<
 { gis1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "-37"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { gis1 ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 |  
<<
 { gis4 r4 b,2^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 | \pageBreak 
<<
 { b,1 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 |  
<<
 { b,16[ bes,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } des16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ des4 ges,2^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-35"} \hide c8 
 d16^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "6/5" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/5" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "-37"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ges,1 ~ }
>>
 | \break \noPageBreak 
<<
 { ges,1 ~ }
>>
 |  
<<
 { ges,1 ~ }
>>
 |  
<<
 { ges,2 ~ ges,8[ r8] r8[ b8^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { b2 fis4^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } a4^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 | \break \noPageBreak 
<<
 { a16[ d8.^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ d8[ g8^\markup { \pad-markup #0.2 "+30"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ g2 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d16^\markup { "-22"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/5" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "-35"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "6/5" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "-37"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { g1 ~ }
>>
 |  
<<
 { g4 ~ g8[ r8] r8[ aes,8^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ aes,4 }
>>
 |  
<<
 { d1^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 | \break \noPageBreak 
<<
 { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.4 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-35"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "6/5" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "-37"} \hide c'8 
 d'16^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/5" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-22"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d2. ~ d8[ r8] }
>>
 |  
<<
 { r8[ fis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } cis16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ cis16[ b,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } e8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] cis2^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 | \break \noPageBreak 
<<
 { cis8[ d8^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ d2. ~ }
>>
 |  
<<
 { d1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "-37"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d'16^\markup { "-22"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "-35"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 ~ }
>>
 |  
<<
 { d2 ~ d16[ r8.] r16[ d8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ges,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ges,4 ~ ges,8[ des8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ des8.[ aes,16^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ aes,16[ ees8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } bes,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] }
>>
 |  
<<
 { ges,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ d8.^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ d16[ bes,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ges,8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ ges,2 ~ }
>>
 |  
<<
 { ges,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-35"} \hide c8 
 e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "-35"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "-37"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ges,1 }
>>
 | \break \noPageBreak 
<<
 { r4 d2.^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { d2. ~ d8[ b,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b,4 ~ b,16[ e,8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,2 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.7 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+14"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "-37"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "-35"} \hide c''8 
 e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e,1 ~ }
>>
 | \break \noPageBreak 
<<
 { e,16[ r8.] r16[ aes,8.^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ aes,8.[ b,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ees16^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ d8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ees16^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { ees16[ aes,8.^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ aes,2. ~ }
>>
 |  
<<
 { aes,4 bes,4^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ bes,8.[ aes,16^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] b,4^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 | \pageBreak 
<<
 { b,1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.8 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-35"} \hide c8 
 e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b,1 ~ }
>>
 |  
<<
 { b,4 ~ b,8.[ r16] r8.[ cis16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ cis4 ~ }
>>
 |  
<<
 { cis4 ~ cis16[ e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } b,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,8[ d16^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } gis,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ gis,16[ ais,8.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ais,4 e,8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ fis,16^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis,16[ d8.^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d16[ b,8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 |  
<<
 { b,2. ~ b,8.[ e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e,2 ais,2^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 \bar "|.|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.1 } 
\stopStaff s4. \startStaff \clef bass s16 
e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+14"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ais,1 ~ }
>>
 |  
<<
 { ais,1 ~ }
>>
 |  
<<
 { ais,8.[ r16] r8.[ cis16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ cis2 ~ }
>>
 |  
<<
 { cis4 ~ cis16[ b,8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b,16[ gis,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } fis,16^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] e,8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ gis,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { gis,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.2 } 
\stopStaff s4. \startStaff \clef bass s16 
e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+16"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+14"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { gis,1 ~ }
>>
 |  
<<
 { gis,16[ r8.] r16[ d8.^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d2 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 | \break \noPageBreak 
<<
 { d1 ~ }
>>
 |  
<<
 { d2 ~ d8.[ ais,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais,16[ gis,8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 |  
<<
 { gis,16[ b,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } gis,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ gis,8.[ e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e,4 cis16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }[ fis,8.^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+14"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-46"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+16"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis,1 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,4 r4 f2^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { f1 }
>>
 | \break \noPageBreak 
<<
 { b,1^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.4 } 
\stopStaff s4. \startStaff \clef bass s16 
b,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+14"} \hide c'8 
 ais16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "-46"} \hide c''8 
 e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b,1 ~ }
>>
 | \break \noPageBreak 
<<
 { b,1 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 |  
<<
 { b,8[ r8] r8[ fis8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ fis4 dis4^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ }
>>
 |  
<<
 { dis1 ~ }
>>
 | \pageBreak 
<<
 { dis1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+16"} \hide c8 
 ais,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
ais16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+14"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { dis1 }
>>
 |  
<<
 { r4 bes,2.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { bes,2 ~ bes,16[ g16^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } c8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ c4 ~ }
>>
 | \break \noPageBreak 
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.6 } 
\stopStaff s4. \startStaff \clef bass s16 
ais,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
ais16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+16"} \hide c''8 
 ais'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c1 ~ }
>>
 | \break \noPageBreak 
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c2. r4 }
>>
 |  
<<
 { ees16^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ f8.^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] g2^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ g8.[ d16^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { d16[ bes,8.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ bes,4 ~ bes,16[ ges8^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } bes,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ bes,8[ c16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } g16^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { g8.[ ees16^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ees2. ~ }
>>
 |  
<<
 { ees1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 8.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais,16^\markup { "-35"} \hide c8 
 dis16^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "22/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
ais16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais'16^\markup { "-35"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ees1 ~ }
>>
 | \break \noPageBreak 
<<
 { ees1 ~ }
>>
 |  
<<
 { ees1 ~ }
>>
 |  
<<
 { ees2 ~ ees8[ r8] r8[ ees8^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { ees1 ~ }
>>
 | \break \noPageBreak 
<<
 { ees1 ~ }
>>
 |  
<<
 { ees1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 8.2 } 
\stopStaff s4. \startStaff \clef bass s16 
dis16^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "22/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais16^\markup { "-35"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+6"} \hide c''8 
 ais'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ees1 ~ }
>>
 |  
<<
 { ees2. ~ ees8[ r8] }
>>
 | \break \noPageBreak 
<<
 { r8[ bes8^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ bes2. ~ }
>>
 |  
<<
 { bes16[ c'8.^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'8.[ ges16^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] bes2^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 8.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 dis16^\markup { "+17"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "22/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
ais'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes1 ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 |  
<<
 { bes4 ~ bes16[ r8.] r16[ fis,8.^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ fis,4 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 | \pageBreak 
<<
 { fis,8[ c8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ c2. ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 8.4 } 
\stopStaff s4. \startStaff \clef bass s16 
fis,16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais'16^\markup { "-35"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c2. ~ c8[ r8] }
>>
 |  
<<
 { r8[ gis,8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ gis,2. ~ }
>>
 |  
<<
 { gis,2 e16^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ fis,8^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } c16^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ c4 ~ }
>>
 | \break \noPageBreak 
<<
 { c16[ a,8.^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ a,8.[ gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ gis,8[ cis16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } d16^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ d8.[ a,16^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { a,4 ~ a,8[ fis,8^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ fis,8[ e16^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } cis16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ cis4 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 | \break \noPageBreak 
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 9.1 } 
\stopStaff s4. \startStaff \clef bass s16 
fis,16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "+6"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
fis'16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis2. r4 }
>>
 | \break \noPageBreak 
<<
 { a,8^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ gis,8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ gis,8[ fis,16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } d16^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ d8.[ cis16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] a,16^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ c8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } cis16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { cis4 gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ fis,8^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } e16^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] a,8^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ d8^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ d4 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 |  
<<
 { d16[ fis,8.^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ fis,2. ~ }
>>
 | \break \noPageBreak 
<<
 { fis,1 ~ }
>>
 |  
<<
 { fis,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 9.2 } 
\stopStaff s4. \startStaff \clef bass s16 
fis,16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+8"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+6"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis,1 ~ }
>>
 |  
<<
 { fis,4 r4 cis8^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ c16^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } a,16^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ a,4 ~ }
>>
 | \break \noPageBreak 
<<
 { a,8[ d16^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ gis,2 ~ gis,16[ a,8.^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { a,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 9.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "+6"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "+8"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { a,1 ~ }
>>
 |  
<<
 { a,1 ~ }
>>
 | \break \noPageBreak 
<<
 { a,1 ~ }
>>
 |  
<<
 { a,4 ~ a,8.[ r16] r8.[ c16^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ c4 ~ }
>>
 |  
<<
 { c4 ~ c16[ aes,8.^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes,16[ ges8.^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ ges4 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 9.4 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+8"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+6"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ges1 ~ }
>>
 | \break \noPageBreak 
<<
 { ges4 ~ ges8.[ r16] r8.[ d16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ d16[ ees8.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { ees8[ aes,8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] e8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }[ aes,8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ees8.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ ges16^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ ges16[ bes,8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { bes,4 ~ bes,16[ c8.^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] d2^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { d1 ~ }
>>
 | \pageBreak 
<<
 { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.1 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 dis'16^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 ~ }
>>
 |  
<<
 { d2 ~ d8[ r8] r8[ aes,8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 | \break \noPageBreak 
<<
 { aes,8[ bes,8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ bes,4 ~ bes,16[ e8.^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }] ~ e4 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+10"} \hide c8 
 dis16^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 dis'16^\markup { "+12"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
d''16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e1 ~ }
>>
 | \break \noPageBreak 
<<
 { e4 r4 des'2^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { des'2. ~ des'8.[ ees16^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { ees1 ~ }
>>
 |  
<<
 { ees4 ~ ees16[ g8.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ g2 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 dis16^\markup { "+12"} \hide c8 
 d16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 d'16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-39"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { g1 ~ }
>>
 |  
<<
 { g2. ~ g8[ r8] }
>>
 |  
<<
 { r8[ e8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ e16[ g8.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ g4 ~ g8.[ a16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { a1 ~ }
>>
 | \break \noPageBreak 
<<
 { a1 ~ }
>>
 |  
<<
 { a1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.4 } 
\stopStaff s4. \startStaff \clef bass s16 
d16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "55/32" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d'16^\markup { "-39"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "55/32" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "55/32" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { a1 ~ }
>>
 |  
<<
 { a1 ~ }
>>
 | \break \noPageBreak 
<<
 { a1 ~ }
>>
 |  
<<
 { a1 ~ }
>>
 |  
<<
 { a4 r4 f4^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } d4^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { d4 ~ d16[ f8.^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ f2 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d16^\markup { "-39"} \hide c8 
 e,16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "55/32" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "55/32" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "55/32" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
d''16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f2 ~ f8[ r8] r8[ fis,8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] }
>>
 | \break \noPageBreak 
<<
 { b,2^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ b,8[ cis8^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ cis4 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.6 } 
\stopStaff s4. \startStaff \clef bass s16 
e,16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-39"} \hide c''8 
 e'16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "55/32" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis8.[ r16] r8.[ d16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d4 ~ d16[ e,8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] }
>>
 | \pageBreak 
<<
 { ais,1^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { ais,1 ~ }
>>
 |  
<<
 { ais,2 gis,2^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ }
>>
 |  
<<
 { gis,16[ d8.^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d2. ~ }
>>
 | \break \noPageBreak 
<<
 { d1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+23"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 ~ }
>>
 |  
<<
 { d2 ~ d8[ r8] r8[ a16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } aes16^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] }
>>
 |  
<<
 { des16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ ees8.^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ ees2. ~ }
>>
 | \break \noPageBreak 
<<
 { ees1 ~ }
>>
 |  
<<
 { ees4 ~ ees8.[ bes16^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ bes2 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.2 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+23"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes1 ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 | \break \noPageBreak 
<<
 { bes2. ~ bes16[ r8.] }
>>
 |  
<<
 { r16[ ees8.^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ ees2. }
>>
 |  
<<
 { des1^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { des1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.3 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-36"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+23"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { des1 ~ }
>>
 |  
<<
 { des1 ~ }
>>
 |  
<<
 { des4 r4 aes8^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ ees8^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ ees8.[ f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 |  
<<
 { f8[ aes8^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ aes16[ bes8.^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ bes8.[ a16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ a16[ des8.^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 | \break \noPageBreak 
<<
 { des16[ ges8.^\markup { \pad-markup #0.2 "+15"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ges2. ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.4 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "-36"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ges1 ~ }
>>
 |  
<<
 { ges1 ~ }
>>
 |  
<<
 { ges4 ~ ges8.[ r16] r8.[ bes,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ bes,8.[ ges16^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ges16[ ees8.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ ees8.[ ges16^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ ges8[ aes,8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes,16[ d8^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } bes,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { bes,8.[ c16^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ges2^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } aes,4^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { aes,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+10"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+23"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "-36"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { aes,1 ~ }
>>
 | \break \noPageBreak 
<<
 { aes,2. ~ aes,8[ r8] }
>>
 |  
<<
 { r8[ aes8^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ aes4 ~ aes16[ a16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } bes16^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ bes4 ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 \bar "||" \pageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "-36"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes1 ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 |  
<<
 { bes2. r4 }
>>
 |  
<<
 { aes,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ ees8.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] c2.^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ }
>>
 | \break \noPageBreak 
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c4 f2.^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.7 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-36"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 | \break \noPageBreak 
<<
 { f1 ~ }
>>
 |  
<<
 { f8.[ r16] r8.[ aes,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] d4^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ges4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { ges16[ ees8.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ ees8[ aes,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } bes,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ bes,16[ ees8.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ ees4 ~ }
>>
 |  
<<
 { ees1 ~ }
>>
 | \break \noPageBreak 
<<
 { ees2 ~ ees8.[ f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ f4 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 \bar "|.|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 12.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+10"} \hide c8 
 ais,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f2 ~ f8[ r8] r8[ bes,8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] }
>>
 | \break \noPageBreak 
<<
 { c1^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 12.2 } 
\stopStaff s4. \startStaff \clef bass s16 
ais,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-50"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c1 ~ }
>>
 | \break \noPageBreak 
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c2. r4 }
>>
 |  
<<
 { d2.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } f4^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 | \break \noPageBreak 
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 12.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais,16^\markup { "+14"} \hide c8 
 f,16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-50"} \hide c'8 
 ais16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 |  
<<
 { f2 ~ f8.[ r16] r8.[ bes,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 | \pageBreak 
<<
 { bes,4 d2^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ d16[ des8.^\markup { \pad-markup #0.2 "-9"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 |  
<<
 { des4 ~ des16[ f,16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } c8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ c2 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 12.4 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais16^\markup { "+14"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c1 ~ }
>>
 | \break \noPageBreak 
<<
 { c16[ r8.] r16[ g,8.^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] d4^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ d16[ c16^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } aes,8^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] }
>>
 |  
<<
 { f,8^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ bes,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } d16^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d16[ f,8.^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f,8[ des8^\markup { \pad-markup #0.2 "-9"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] c8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ f,8^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { f,1 ~ }
>>
 |  
<<
 { f,1 ~ }
>>
 | \break \noPageBreak 
<<
 { f,8[ d8^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d2 bes,4^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { bes,1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f,16^\markup { "-50"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes,2. ~ bes,8[ r8] }
>>
 |  
<<
 { r8[ dis8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ dis8[ ais,8^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ ais,2 }
>>
 | \break \noPageBreak 
<<
 { fis8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ gis,8^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ gis,16[ f8.^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ f2 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.2 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-50"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 | \break \noPageBreak 
<<
 { f4 r4 c2^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 |  
<<
 { c2. gis,4^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } }
>>
 |  
<<
 { dis1^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+37"} \hide c8 
 g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "10/9" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "-46"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "10/9" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { dis1 ~ }
>>
 | \break \noPageBreak 
<<
 { dis1 ~ }
>>
 |  
<<
 { dis1 ~ }
>>
 |  
<<
 { dis1 ~ }
>>
 |  
<<
 { dis16[ r8.] r16[ ais,8.^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ ais,8.[ g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ g,8.[ d16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 | \break \noPageBreak 
<<
 { d16[ e8.^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ e4 d2^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { d1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.4 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-50"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+37"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "10/9" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 | \break \noPageBreak 
<<
 { d2 r4 a,4^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 |  
<<
 { a,4 g,4^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ g,8[ dis16^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ais,16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ ais,4 ~ }
>>
 |  
<<
 { ais,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.5 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
gis16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ais,1 ~ }
>>
 | \pageBreak 
<<
 { ais,4 ~ ais,8.[ r16] r8.[ c16^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ c8.[ d16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { d8[ g,8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ g,8[ e8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ e16[ dis8^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } c16^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ dis16^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } c8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { c4 ~ c16[ d8.^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ d2 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.6 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+37"} \hide c'8 
 g16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "10/9" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d2. ~ d8[ r8] }
>>
 | \break \noPageBreak 
<<
 { r8[ ais,8^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ ais,2. ~ }
>>
 |  
<<
 { ais,2. ~ ais,8.[ e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { e2 a,16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ g,8.^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ g,16[ a,16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } e8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] }
>>
 |  
<<
 { dis4^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } a,8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } c16^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ais,8.^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] d4^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } }
>>
 | \break \noPageBreak 
<<
 { g,8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ dis8^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ dis8[ c8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ c2 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 |  
<<
 { c1 ~ }
>>
 | \break \noPageBreak 
<<
 { c1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 14.1 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g16^\markup { "-46"} \hide c'8 
 d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c1 ~ }
>>
 |  
<<
 { c2. ~ c8[ r8] }
>>
 |  
<<
 { r8[ a,8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ a,2. ~ }
>>
 | \break \noPageBreak 
<<
 { a,1 ~ }
>>
 |  
<<
 { a,1 ~ }
>>
 |  
<<
 { a,4 ~ a,8[ d8^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ d2 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 14.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g,16^\markup { "-46"} \hide c8 
 a,16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 ~ }
>>
 | \break \noPageBreak 
<<
 { d2 r4 c4^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ }
>>
 |  
<<
 { c4 ~ c16[ b,8.^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] d2^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } }
>>
 |  
<<
 { f4^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } a,8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ e8^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ e2 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 14.3 } 
\stopStaff s4. \startStaff \clef bass s16 
a,16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "-46"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e1 ~ }
>>
 | \break \noPageBreak 
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e4 ~ e16[ r8.] r16[ fis8.^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ fis4 ~ }
>>
 | \break \noPageBreak 
<<
 { fis4 ~ fis8[ d8^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ d2 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 14.4 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 a,16^\markup { "-42"} \hide c8 
 d16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
d''16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 | \pageBreak 
<<
 { d1 ~ }
>>
 |  
<<
 { d8[ r8] r8[ f8^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ f2 ~ }
>>
 |  
<<
 { f2 e2^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 |  
<<
 { e1 ~ }
>>
 | \break \noPageBreak 
<<
 { e2. ~ e8[ d8^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { d1 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.1 } 
\stopStaff s4. \startStaff \clef bass s16 
d16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d'16^\markup { "-44"} \hide c'8 
 a16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-44"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 ~ }
>>
 | \break \noPageBreak 
<<
 { d1 ~ }
>>
 |  
<<
 { d2. ~ d16[ r8.] }
>>
 |  
<<
 { r16[ b8.^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b2. ~ }
>>
 |  
<<
 { b16[ g8.^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ g2. ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d16^\markup { "-44"} \hide c8 
 g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
a16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "+7"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { g1 ~ }
>>
 |  
<<
 { g2 r4 d4^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } }
>>
 |  
<<
 { ees2^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ ees8[ g,8^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] d4^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { d16[ f8.^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] bes,4^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ d16^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } a,8^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ a,8.[ ees16^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ees1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.3 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 a16^\markup { "-42"} \hide c'8 
 g16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-44"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ees1 ~ }
>>
 |  
<<
 { ees1 ~ }
>>
 |  
<<
 { ees1 ~ }
>>
 | \break \noPageBreak 
<<
 { ees4 r4 bes,16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } d8^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ d4 ~ }
>>
 |  
<<
 { d1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.4 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g16^\markup { "+7"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+25"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d1 }
>>
 |  
<<
 { r4 a,8^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ ees8^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] g,2^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 | \break \noPageBreak 
<<
 { g,2 f8.^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ ees16^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] bes,8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ d8^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { d4 ~ d16[ a,8.^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ a,2 ~ }
>>
 |  
<<
 { a,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g,16^\markup { "+7"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { a,1 ~ }
>>
 | \break \noPageBreak 
<<
 { a,4 ~ a,8.[ r16] r8.[ f16^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ f4 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f2 ~ f8.[ gis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ gis4 ~ }
>>
 |  
<<
 { gis4 ~ gis8[ b,8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b,2 ~ }
>>
 | \pageBreak 
<<
 { b,1 ~ }
>>
 |  
<<
 { b,1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+25"} \hide c8 
 g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "+7"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b,1 }
>>
 |  
<<
 { r4 d2.^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 | \break \noPageBreak 
<<
 { d2 ~ d16[ f8.^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ f16[ bes,16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } f16^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { g,8[ ees8^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ ees8.[ a,16^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] bes,8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ f8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ f4 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.7 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g,16^\markup { "+7"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+25"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 | \break \noPageBreak 
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f2 ~ f8[ r8] r8[ f8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { f1 ~ }
>>
 \bar ".|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.8 } 
\stopStaff s4. \startStaff \clef bass s16 
b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "+7"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 |  
<<
 { f16[ r8.] r16[ b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } gis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } fis16^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ fis4 ~ fis8[ dis8^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 |  
<<
 { dis8[ cis8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ cis4 fis16^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } a8^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ a16[ dis16^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } fis8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] }
>>
 |  
<<
 { b,8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ cis8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ cis4 f8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ fis8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ fis16[ b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } gis8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] }
>>
 | \break \noPageBreak 
<<
 { dis1^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ }
>>
 |  
<<
 { dis1 ~ }
>>
 |  
<<
 { dis1 ~ }
>>
 |  
<<
 { dis4 ~ dis8.[ cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ cis2 ~ }
>>
 | \break \noPageBreak 
<<
 { cis4 ~ cis16[ b,8.^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b,2 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+25"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b,1 ~ }
>>
 |  
<<
 { b,4 r4 b2^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 |  
<<
 { b8[ ais8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ dis8.^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ dis2 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "+29"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+25"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { dis1 ~ }
>>
 | \break \noPageBreak 
<<
 { dis1 ~ }
>>
 |  
<<
 { dis1 ~ }
>>
 |  
<<
 { dis1 ~ }
>>
 |  
<<
 { dis2 r4 f4^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 | \pageBreak 
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+25"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+29"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 |  
<<
 { f4 ~ f8.[ r16] r8.[ b16^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b4 ~ }
>>
 | \break \noPageBreak 
<<
 { b4 ~ b16[ e8.^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ e2 ~ }
>>
 |  
<<
 { e2 ~ e8[ cis8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ cis4 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 | \break \noPageBreak 
<<
 { cis1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.4 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+25"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 | \break \noPageBreak 
<<
 { cis2 ~ cis8[ r8] r8[ gis8^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { gis2. ~ gis16[ cis8.^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.5 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+25"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "-35"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis16[ r8.] r16[ b8.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b8.[ ais16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ ais8[ dis8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { dis4 e8^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ g16^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } b16^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] cis2^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "+29"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "-35"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+25"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis1 ~ }
>>
 |  
<<
 { cis8[ r8] r8[ ees16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } bes,16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ bes,2 ~ }
>>
 |  
<<
 { bes,2 ~ bes,8[ ees8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ ees4 ~ }
>>
 | \break \noPageBreak 
<<
 { ees2 ~ ees8.[ e16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ e4 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.7 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "-35"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+29"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "+29"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e2. ~ e8.[ r16] }
>>
 |  
<<
 { r8.[ gis16^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ gis2 ~ gis16[ e8.^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { e4 g16^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ b16^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } dis8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ dis4 cis4^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 | \break \noPageBreak 
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 \bar "||" \pageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.8 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "+29"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "-35"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis1 ~ }
>>
 |  
<<
 { cis1 ~ }
>>
 |  
<<
 { cis2. ~ cis8[ r8] }
>>
 |  
<<
 { r8[ e8^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] des4^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ des8.[ ees16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ ees16[ f8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } c16^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { c4 ees8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ des8^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ des2 }
>>
 |  
<<
 { bes,1^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.9 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes,1 ~ }
>>
 |  
<<
 { bes,8[ r8] r8[ aes,8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes,2 ~ }
>>
 | \break \noPageBreak 
<<
 { aes,1 ~ }
>>
 |  
<<
 { aes,1 ~ }
>>
 |  
<<
 { aes,1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.10 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+29"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { aes,1 ~ }
>>
 | \break \noPageBreak 
<<
 { aes,1 ~ }
>>
 |  
<<
 { aes,2. ~ aes,8[ r8] }
>>
 |  
<<
 { r8[ f16^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } e16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ e8.[ f16^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ees4^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ ees8[ des8^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] }
>>
 |  
<<
 { c8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ ees8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ ees4 e2^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 | \break \noPageBreak 
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 | \break \noPageBreak 
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e1 \fermata  }>> \bar "|." 
} 

>>
>>