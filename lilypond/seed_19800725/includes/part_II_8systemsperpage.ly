\new StaffGroup  \with {\remove "System_start_delimiter_engraver"}
<<
\new Staff = "II" \with { 
instrumentName = "II" 
shortInstrumentName = "II" 
midiInstrument = #"clarinet" 

}
<<

{  \tempo 2 = 60
 \numericTimeSignature \time 2/2
 \clef alto
  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.1 } 
\stopStaff s4. \startStaff \clef bass s16 
e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { r1  }
>>
 |  
<<
 { r1  }
>>
 |  
<<
 { r1  }
>>
 |  
<<
 { r1  }
>>
 | \break \noPageBreak 
<<
 { r1  }
>>
 |  
<<
 { r2.  r8[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.2 } 
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b16[ r8.] r16[ e8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 ais4^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.3 } 
 { ais1 ~ }
>>
 |  
<<
 { ais2 ~ ais16[ r8.] r16[ e8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e4 d'4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ d'8.[ fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] }
>>
 |  
<<
 { b8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'4 c'8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }[ g16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g16[ ais16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } g16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 | \break \noPageBreak 
<<
 { g8[ c'8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'2. ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.4 } 
 { c'2. ~ c'8[ r8] }
>>
 |  
<<
 { r8[ fis8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis2. ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis2 ~ fis8[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b8[ ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { ais16[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e8.[ ais16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais8.[ fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.5 } 
 { fis1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis2 ~ fis16[ r8.] r16[ g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b4 ~ b8[ d'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'4 e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] }
>>
 |  
<<
 { d'4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } g2^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ g16[ ais8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.6 } 
 { ais1 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 | \pageBreak 
<<
 { ais1 }
>>
 |  
<<
 { r4 c'4^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ c'16[ e8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.7 } 
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 | \break \noPageBreak 
<<
 { e1 ~ }
>>
 |  
<<
 { e4 ~ e16[ r8.] r16[ fis8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis8.[ d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.8 } 
 { d'1 ~ }
>>
 |  
<<
 { d'4 ~ d'8[ r8] r8[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b4 ~ }
>>
 |  
<<
 { b4 ais4^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } b2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 | \break \noPageBreak 
<<
 { b1 ~ }
>>
 |  
<<
 { b2. ~ b8.[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e1 }
>>
 |  
<<
 { c'1^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.9 } 
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'4 ~ c'16[ r8.] r16[ d'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis8[ ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { ais16[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] g4^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ g8.[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'16[ ais8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ais8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e16[ d'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ fis8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis4 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.10 } 
 { fis1 ~ }
>>
 |  
<<
 { fis4 r4 d'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e8[ b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.11 } 
 { d'1 ~ }
>>
 |  
<<
 { d'4 ~ d'8.[ r16] r8.[ fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis8.[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 | \break \noPageBreak 
<<
 { e4 ~ e8[ c'8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'4 ~ c'8.[ ais16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { ais2 ~ ais16[ e8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] g4^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.12 } 
 { g1 ~ }
>>
 |  
<<
 { g1 }
>>
 | \pageBreak 
<<
 { r4 d'4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ d'8[ c'8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] }
>>
 |  
<<
 { d'2^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ d'16[ g8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g4 ~ }
>>
 |  
<<
 { g1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.13 } 
 { g1 ~ }
>>
 | \break \noPageBreak 
<<
 { g2. ~ g8.[ r16] }
>>
 |  
<<
 { r8.[ b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b2 ~ b8.[ fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e16[ b8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b2 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b2. ~ b8[ g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { g1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.14 } 
 { g1 ~ }
>>
 |  
<<
 { g4 r4 ais16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ d'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'16[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ais4 g16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] d'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ ais16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] e4^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { e16[ c'8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis16[ d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e2 ~ }
>>
 |  
<<
 { e4 ~ e16[ g8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g2 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.15 } 
 { g1 ~ }
>>
 | \break \noPageBreak 
<<
 { g2 ~ g8.[ r16] r8.[ d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.16 } 
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'4 ~ d'8.[ r16] r8.[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'4 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.17 } 
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'4 ~ c'8[ r8] r8[ g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g16[ ais8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 | \pageBreak 
<<
 { ais1 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.18 } 
 { ais2. ~ ais8[ r8] }
>>
 |  
<<
 { r8[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b4 ~ b8.[ fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis4 ~ }
>>
 | \break \noPageBreak 
<<
 { fis1 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.19 } 
 { fis1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis8.[ r16] r8.[ d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'2 ~ }
>>
 |  
<<
 { d'1 }
>>
 |  
<<
 { c'1^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.20 } 
 { c'1 ~ }
>>
 |  
<<
 { c'2. r4 }
>>
 |  
<<
 { ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b8[ ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] g8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e16[ d'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { fis1 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.21 } 
 { fis1 ~ }
>>
 |  
<<
 { fis4 ~ fis8.[ r16] r8.[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'4 ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 |  
<<
 { c'2. ~ c'8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.22 } 
 { e1 ~ }
>>
 | \break \noPageBreak 
<<
 { e4 r4 g2^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 |  
<<
 { g2 ~ g16[ d'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'8.[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 |  
<<
 { c'2 ~ c'16[ b8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b4 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.23 } 
 { b1 ~ }
>>
 |  
<<
 { b16[ r8.] r16[ fis8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis2 ~ }
>>
 |  
<<
 { fis4 ~ fis8.[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 c'8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }[ ais16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 | \pageBreak 
<<
 { ais8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e2 ~ e16[ d'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'4 ~ d'8[ fis8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ais2^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.24 } 
 { ais1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais2. ~ ais16[ r8.] }
>>
 |  
<<
 { r16[ e8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e8[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b4 d'4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } }
>>
 |  
<<
 { e1^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { e1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.25 } 
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e4 r4 c'2^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.26 } 
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 |  
<<
 { c'16[ r8.] r16[ fis8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis2 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 |  
<<
 { fis2 g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b4 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.27 } 
 { b1 ~ }
>>
 |  
<<
 { b1 }
>>
 |  
<<
 { r4 ais2.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { ais4 ~ ais8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e8[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b4 }
>>
 | \break \noPageBreak 
<<
 { d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ g8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g16[ b8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b2 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.28 } 
 { b1 ~ }
>>
 |  
<<
 { b2 r4 c'4^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 | \break \noPageBreak 
<<
 { c'4 ~ c'16[ fis8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 ~ }
>>
 |  
<<
 { e16[ b8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g2 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.29 } 
 { g1 ~ }
>>
 |  
<<
 { g1 ~ }
>>
 | \pageBreak 
<<
 { g2 ~ g16[ r8.] r16[ c'8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 |  
<<
 { c'2 fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ b8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b8[ ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { ais16[ e8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e2. ~ }
>>
 |  
<<
 { e1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.30 } 
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e4 ~ e16[ r8.] r16[ g8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g4 ~ }
>>
 | \break \noPageBreak 
<<
 { g8.[ d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'2. ~ }
>>
 |  
<<
 { d'8.[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'16[ fis8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis2 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.31 } 
 { fis1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis2. ~ fis8[ r8] }
>>
 |  
<<
 { r8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e2 ais16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ fis8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b16[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 d'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ g16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g4 ~ }
>>
 |  
<<
 { g16[ b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g2. ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.32 } 
 { g1 ~ }
>>
 |  
<<
 { g4 ~ g8[ r8] r8[ c'8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'16[ fis8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis16[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] c'4^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ c'16[ ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 ~ }
>>
 |  
<<
 { e1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.33 } 
 { e1 ~ }
>>
 |  
<<
 { e4 ~ e8.[ r16] r8.[ g16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g4 ~ }
>>
 |  
<<
 { g2. ~ g16[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis16[ d'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ais4^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } fis8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g4 ~ }
>>
 | \break \noPageBreak 
<<
 { g1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.34 } 
 { g1 ~ }
>>
 |  
<<
 { g2 ~ g8.[ r16] r8.[ d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'8.[ fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis4 e2^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 | \break \noPageBreak 
<<
 { e8.[ b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b8[ c'8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'4 ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ fis8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.35 } 
 { fis1 ~ }
>>
 | \pageBreak 
<<
 { fis16[ r8.] r16[ c'8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'4 b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ d'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'2 ~ d'16[ e8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 ~ }
>>
 |  
<<
 { e8[ g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g2. ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.36 } 
 { g1 }
>>
 | \break \noPageBreak 
<<
 { r4 b2.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { b2. ais4^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais2 ~ ais16[ d'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'4 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.37 } 
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'4 ~ d'8[ r8] r8[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b16[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] }
>>
 | \break \noPageBreak 
<<
 { d'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g4 c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }[ b8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b4 }
>>
 |  
<<
 { d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ fis8.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ais8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b2 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.38 } 
 { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 |  
<<
 { b2. r4 }
>>
 |  
<<
 { e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais8[ g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g2 ~ }
>>
 | \break \noPageBreak 
<<
 { g1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.39 } 
 { g1 ~ }
>>
 |  
<<
 { g1 ~ }
>>
 |  
<<
 { g1 ~ }
>>
 | \break \noPageBreak 
<<
 { g4 ~ g8[ r8] r8[ b8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b16[ fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } d'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'8.[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 ~ e16[ b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } fis8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis4 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.40 } 
 { fis1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis2 ~ fis8.[ r16] r8.[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 |  
<<
 { c'4 ~ c'8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 ~ e16[ g8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { g1 ~ }
>>
 |  
<<
 { g1 ~ }
>>
 | \pageBreak 
<<
 { g2. ~ g16[ ais8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.41 } 
 { ais1 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais1 ~ }
>>
 |  
<<
 { ais2. r4 }
>>
 |  
<<
 { c'4^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ c'8[ g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] e2^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 \bar "||"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.42 } 
 { e1 ~ }
>>
 | \break \noPageBreak 
<<
 { e1 ~ }
>>
 |  
<<
 { e2 r4 g4^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 |  
<<
 { g2. ~ g8.[ d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 \bar ".|"  
<<
\once \override Score.RehearsalMark.self-alignment-X = #0 
 \once \override Score.RehearsalMark.Y-offset = #5 
 \once \override Score.RehearsalMark.X-offset = #1 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 1.43 } 
 { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'4 r4 b2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { b1 ~ }
>>
 |  
<<
 { b8.[ fis16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis2. ~ }
>>
 | \break \noPageBreak 
<<
 { fis1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+0"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+0"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis1 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 |  
<<
 { fis8.[ r16] r8.[ c'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ c'2 ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.2 } 
\stopStaff s4. \startStaff \clef bass s16 
fis,16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+0"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+2"} \hide c''8 
 e'16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 | \break \noPageBreak 
<<
 { c'2. r4 }
>>
 |  
<<
 { b1^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "+4"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+2"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+0"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b4 ~ b8[ r8] r8[ ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] e8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ d'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'2 ~ d'8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] fis4^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 |  
<<
 { fis4 ~ fis8[ ais8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais8.[ b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b4 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.4 } 
\stopStaff s4. \startStaff \clef bass s16 
b,16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+4"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b2. ~ b8[ r8] }
>>
 | \pageBreak 
<<
 { r8[ g8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ g2. ~ }
>>
 |  
<<
 { g4 b2.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+2"} \hide c8 
 e,16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+0"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+2"} \hide c''8 
 ais'16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b16[ r8.] r16[ a'8.^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ a'4 g'4^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { g'2. ~ g'16[ d'8.^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 2.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+0"} \hide c8 
 ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+2"} \hide c'8 
 ais16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
ais'16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'4 ~ d'16[ r8.] r16[ bes8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } c'16^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ c'16[ des'8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ees'16^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { ees'4 bes16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ ges'8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ees'16^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ees'8[ bes8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ bes16[ f'8.^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 | \break \noPageBreak 
<<
 { f'4 des'8.^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ g'16^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ g'16[ ees'8.^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ges'4^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { ges'2 ~ ges'8[ c'8^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ c'4 ~ }
>>
 |  
<<
 { c'8[ bes8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ bes2. ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 \bar "|.|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.1 } 
\stopStaff s4. \startStaff \clef bass s16 
ais,16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais16^\markup { "-49"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais'16^\markup { "-49"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes1 ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 |  
<<
 { bes4 ~ bes8[ r8] r8[ b8^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ b4 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b1 ~ }
>>
 |  
<<
 { b2 ~ b8[ ges8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ ges4 ~ }
>>
 |  
<<
 { ges1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais,16^\markup { "-49"} \hide c8 
 f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
fis16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ges1 ~ }
>>
 | \break \noPageBreak 
<<
 { ges1 ~ }
>>
 |  
<<
 { ges2. r4 }
>>
 |  
<<
 { des'1^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { des'8[ ges8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ ges16[ e'8.^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ e'2 ~ }
>>
 | \pageBreak 
<<
 { e'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.3 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "-8"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-47"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e'2. ~ e'8[ r8] }
>>
 |  
<<
 { r8[ f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } des'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ des'16[ g8.^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] bes8.^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f4 ~ }
>>
 |  
<<
 { f8.[ g16^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ g4 ~ g8.[ c'16^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ c'8[ d'8^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.4 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-47"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "-8"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'2 ~ d'8[ r8] r8[ ges8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { ges4 ~ ges16[ d'16^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } bes8^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ bes2 ~ }
>>
 |  
<<
 { bes4 e'2^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ e'16[ ges8.^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { ges1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.5 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "-8"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-47"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ges1 ~ }
>>
 |  
<<
 { ges2. ~ ges8.[ r16] }
>>
 |  
<<
 { r8.[ c'16^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] aes2.^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } }
>>
 |  
<<
 { c'1^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f,16^\markup { "-47"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "-8"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c'1 ~ }
>>
 | \break \noPageBreak 
<<
 { c'8.[ r16] r8.[ bes16^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] g4^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } c'16^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } bes8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { bes8[ des'8^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] d'2^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } c'4^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 \bar ".|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 3.7 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-8"} \hide c8 
 f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c'1 ~ }
>>
 |  
<<
 { c'16[ r8.] r16[ f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } aes16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } g16^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ g4 des'4^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { des'4 d'2^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ d'8[ bes16^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } g16^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { g4 c'2.^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 | \break \noPageBreak 
<<
 { c'16[ f8.^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f2. ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f2 aes4^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ aes16[ d'8.^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \pageBreak 
<<
 { d'1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.1 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-47"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'4 ~ d'8.[ r16] r8.[ aes16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes4 ~ }
>>
 |  
<<
 { aes2 f'2^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f,16^\markup { "-47"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+40"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f'1 ~ }
>>
 | \break \noPageBreak 
<<
 { f'8[ r8] r8[ gis'8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] e'2^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 |  
<<
 { e'2. ~ e'8.[ fis'16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.3 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-6"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-47"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis'1 ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'4 ~ fis'8.[ r16] r8.[ g16^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ g8[ f8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] }
>>
 | \break \noPageBreak 
<<
 { c'4^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ c'8[ d'8^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'8[ f8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f4 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 |  
<<
 { f1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.4 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-47"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "-6"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 |  
<<
 { f4 ~ f8.[ r16] r8.[ fis'16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] a'8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }[ b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } e'16^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { e'16[ cis'8.^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ cis'8[ a'8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ a'8[ gis'8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ gis'8[ cis'8^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { cis'1 ~ }
>>
 | \break \noPageBreak 
<<
 { cis'1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 4.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+40"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
cis'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-47"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis'1 ~ }
>>
 |  
<<
 { cis'1 ~ }
>>
 |  
<<
 { cis'2 r4 b'4^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 | \break \noPageBreak 
<<
 { b'1 ~ }
>>
 |  
<<
 { b'2 ~ b'8.[ gis'16^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ gis'4 ~ }
>>
 |  
<<
 { gis'4 ~ gis'16[ dis'8.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ dis'8.[ a'16^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ a'8.[ fis'16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] }
>>
 |  
<<
 { cis'4^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } e'8.^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ fis'16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ fis'4 cis'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ gis'8.^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 | \pageBreak 
<<
 { gis'1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 5.1 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/7" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-6"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "-6"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { gis'1 ~ }
>>
 |  
<<
 { gis'1 ~ }
>>
 |  
<<
 { gis'2. ~ gis'8[ r8] }
>>
 | \break \noPageBreak 
<<
 { r8[ bes16^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ges16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ ges16[ c'8.^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ c'16[ e'8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ e'4 ~ }
>>
 |  
<<
 { e'2. ~ e'8.[ ees'16^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 |  
<<
 { ees'1 ~ }
>>
 |  
<<
 { ees'1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 5.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "-6"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/7" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "+45"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "-37"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ees'1 ~ }
>>
 |  
<<
 { ees'4 ~ ees'8[ r8] r8[ dis'8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ dis'4 ~ }
>>
 |  
<<
 { dis'16[ b'8.^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b'16[ gis'8.^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ gis'2 ~ }
>>
 |  
<<
 { gis'1 ~ }
>>
 | \break \noPageBreak 
<<
 { gis'1 ~ }
>>
 |  
<<
 { gis'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 5.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "-37"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/7" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/7" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-6"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+45"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { gis'2. ~ gis'16[ r8.] }
>>
 |  
<<
 { r16[ b8.^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b4 cis'2^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 | \break \noPageBreak 
<<
 { cis'1 ~ }
>>
 |  
<<
 { cis'1 ~ }
>>
 |  
<<
 { cis'1 ~ }
>>
 |  
<<
 { cis'1 ~ }
>>
 \bar ".|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 5.4 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "-6"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis'2. ~ cis'16[ r8.] }
>>
 |  
<<
 { r16[ d'8.^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ d'16[ e'8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] gis'16^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ fis'8.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ fis'4 ~ }
>>
 |  
<<
 { fis'8.[ g'16^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ g'4 ~ g'8.[ b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b16[ d'8.^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ }
>>
 |  
<<
 { d'8.[ cis'16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ cis'8.[ e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ e'2 ~ }
>>
 | \break \noPageBreak 
<<
 { e'1 ~ }
>>
 |  
<<
 { e'4 ~ e'8.[ fis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ fis'2 ~ }
>>
 |  
<<
 { fis'16[ g'8.^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ g'2. ~ }
>>
 |  
<<
 { g'1 ~ }
>>
 | \break \noPageBreak 
<<
 { g'2. d'4^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "-37"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \pageBreak 
<<
 { d'4 ~ d'8.[ r16] r8.[ fis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ fis'8[ b8^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { b4 ~ b16[ cis'8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ e'8.[ g'16^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] b4^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { b4 fis'2.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-35"} \hide c8 
 d16^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "6/5" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/5" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "-37"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis'1 ~ }
>>
 | \break \noPageBreak 
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'2 ~ fis'8[ r8] r8[ ees'8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ees'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d16^\markup { "-22"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/5" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "-35"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "6/5" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "-37"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ees'1 ~ }
>>
 |  
<<
 { ees'4 r4 b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ d'8^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } fis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ fis'8[ e'8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { e'8[ g'8^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ g'4 d'4^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } gis'4^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 | \break \noPageBreak 
<<
 { gis'1 ~ }
>>
 |  
<<
 { gis'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.4 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-35"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "6/5" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "-37"} \hide c'8 
 d'16^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "8/5" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-22"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { gis'1 ~ }
>>
 |  
<<
 { gis'4 ~ gis'8[ r8] r8[ d'16^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } fis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ fis'8[ g'16^\markup { \pad-markup #0.2 "+30"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } b'16^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { b'2 fis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ b'8.^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b'4 ~ }
>>
 |  
<<
 { b'8[ e'8^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ e'2. ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "-37"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d'16^\markup { "-22"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "-35"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e'2. r4 }
>>
 |  
<<
 { ges1^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 | \break \noPageBreak 
<<
 { ges1 ~ }
>>
 |  
<<
 { ges8.[ b16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ b8.[ des'16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ des'8.[ d'16^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ d'8.[ bes16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-35"} \hide c8 
 e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "-35"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "-37"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes1 }
>>
 | \break \noPageBreak 
<<
 { r4 e'2.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { e'1 ~ }
>>
 |  
<<
 { e'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.7 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+14"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "-37"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "-35"} \hide c''8 
 e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e'1 ~ }
>>
 | \break \noPageBreak 
<<
 { e'1 ~ }
>>
 |  
<<
 { e'1 }
>>
 |  
<<
 { r4 cis'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }[ gis8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ gis2 }
>>
 |  
<<
 { e2.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ e8[ fis8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 | \pageBreak 
<<
 { fis4 d'2.^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 6.8 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "-35"} \hide c8 
 e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'2. r4 }
>>
 |  
<<
 { e16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ b16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ais16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } fis16^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] gis2^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ gis16[ e8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { e1 ~ }
>>
 |  
<<
 { e8[ fis8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis2. ~ }
>>
 \bar "|.|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.1 } 
\stopStaff s4. \startStaff \clef bass s16 
e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+14"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis1 ~ }
>>
 |  
<<
 { fis2. ~ fis8.[ r16] }
>>
 |  
<<
 { r8.[ f'16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ f'4 b8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ gis'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } a'16^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ a'16[ dis'8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 |  
<<
 { dis'1 ~ }
>>
 | \break \noPageBreak 
<<
 { dis'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.2 } 
\stopStaff s4. \startStaff \clef bass s16 
e,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+16"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+14"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { dis'1 ~ }
>>
 |  
<<
 { dis'1 ~ }
>>
 |  
<<
 { dis'1 ~ }
>>
 | \break \noPageBreak 
<<
 { dis'1 ~ }
>>
 |  
<<
 { dis'8[ r8] r8[ gis'8^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ gis'2 ~ }
>>
 |  
<<
 { gis'2. ~ gis'16[ cis'8.^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { cis'1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+14"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/12" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-46"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+16"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/12" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis'1 ~ }
>>
 |  
<<
 { cis'2. ~ cis'8.[ r16] }
>>
 |  
<<
 { r8.[ cis'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ cis'2. ~ }
>>
 |  
<<
 { cis'2. ~ cis'16[ ais8.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ais4 ~ ais8[ gis8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ gis4 ~ gis8[ cis'8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] }
>>
 |  
<<
 { e8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ fis8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis4 ~ fis8[ b8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b4 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.4 } 
\stopStaff s4. \startStaff \clef bass s16 
b,16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+14"} \hide c'8 
 ais16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "-46"} \hide c''8 
 e'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b1 }
>>
 | \break \noPageBreak 
<<
 { r4 bes16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ ges'8.^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] bes8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ c'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ees'16^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] c'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ bes8.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { bes2 ~ bes16[ d'8.^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ d'4 }
>>
 |  
<<
 { f'1^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 | \pageBreak 
<<
 { f'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+16"} \hide c8 
 ais,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
ais16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+14"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 |  
<<
 { f'2 ~ f'16[ r8.] r16[ ges'8.^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] }
>>
 | \break \noPageBreak 
<<
 { f'8.^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ c'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] d'8.^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ bes16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] g'16^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ f'8.^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ f'16[ c'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 7.6 } 
\stopStaff s4. \startStaff \clef bass s16 
ais,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
ais16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+16"} \hide c''8 
 ais'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c'1 ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 |  
<<
 { c'16[ r8.] r16[ ees'8.^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] d'8.^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ f'16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ f'8[ g'8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { g'2 ~ g'8[ bes8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ bes4 ~ }
>>
 |  
<<
 { bes2 f'2^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 | \break \noPageBreak 
<<
 { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 8.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais,16^\markup { "-35"} \hide c8 
 dis16^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "22/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
ais16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais'16^\markup { "-35"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f'1 ~ }
>>
 | \break \noPageBreak 
<<
 { f'2. ~ f'16[ r8.] }
>>
 |  
<<
 { r16[ ges'8.^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ ges'16[ ees'8.^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ees'2 ~ }
>>
 |  
<<
 { ees'4 ~ ees'8[ g'16^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } c'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ c'2 }
>>
 |  
<<
 { f'16^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ ees'16^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } bes8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] d'8.^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ ges'16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] g'16^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ f'8.^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ f'4 }
>>
 | \break \noPageBreak 
<<
 { c'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ f'8.^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ f'16[ c'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ c'2 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 8.2 } 
\stopStaff s4. \startStaff \clef bass s16 
dis16^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "22/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais16^\markup { "-35"} \hide c'8 
 fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+6"} \hide c''8 
 ais'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 | \break \noPageBreak 
<<
 { c'2 ~ c'8.[ r16] r8.[ fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] }
>>
 |  
<<
 { c'4^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } fis4^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ fis16[ a8.^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ a4 ~ }
>>
 |  
<<
 { a2 ~ a8.[ cis'16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ cis'4 ~ }
>>
 |  
<<
 { cis'1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 8.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 dis16^\markup { "+17"} \hide c8 
 fis,16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "22/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
ais'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis'2. ~ cis'8[ r8] }
>>
 |  
<<
 { r8[ d'8^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ d'2. ~ }
>>
 |  
<<
 { d'2. fis4^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } }
>>
 |  
<<
 { e'1^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 | \pageBreak 
<<
 { e'1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 8.4 } 
\stopStaff s4. \startStaff \clef bass s16 
fis,16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais'16^\markup { "-35"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e'1 ~ }
>>
 |  
<<
 { e'1 ~ }
>>
 |  
<<
 { e'2. ~ e'8.[ r16] }
>>
 | \break \noPageBreak 
<<
 { r8.[ a16^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ a2. ~ }
>>
 |  
<<
 { a1 ~ }
>>
 |  
<<
 { a2. ~ a16[ c'8.^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { c'4 fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ gis8.^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ gis4 ~ gis16[ cis'16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } e'16^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { e'8[ a8^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] c'8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ e'16^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } fis16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ fis8.[ gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ gis4 ~ }
>>
 |  
<<
 { gis8[ a8^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ a8.[ d'16^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ d'2 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 9.1 } 
\stopStaff s4. \startStaff \clef bass s16 
fis,16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis16^\markup { "+6"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
fis'16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'4 ~ d'8[ r8] r8[ g'16^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } cis'16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ cis'8.[ dis'16^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] }
>>
 |  
<<
 { b'4^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ b'16[ gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } a'8^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ a'4 ~ a'16[ cis'8^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { gis'8[ g'8^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] dis'4^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ dis'16[ cis'8.^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ cis'4 ~ }
>>
 | \break \noPageBreak 
<<
 { cis'1 ~ }
>>
 |  
<<
 { cis'4 ~ cis'16[ a'8.^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ a'2 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 9.2 } 
\stopStaff s4. \startStaff \clef bass s16 
fis,16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+8"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+6"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { a'1 ~ }
>>
 |  
<<
 { a'4 ~ a'8.[ r16] r8.[ ees'16^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ ees'16[ c'8.^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 9.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis,16^\markup { "+6"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "+8"} \hide c''8 
 fis'16^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 | \break \noPageBreak 
<<
 { c'2 ~ c'8[ r8] r8[ e'16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } b'16^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { b'4 ~ b'8[ dis'8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ dis'8[ g'16^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] cis'4^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } }
>>
 |  
<<
 { dis'1^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 9.4 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+8"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 fis'16^\markup { "+6"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { dis'2. ~ dis'16[ r8.] }
>>
 | \break \noPageBreak 
<<
 { r16[ d'8.^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ d'2. ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'2. ~ d'16[ aes8.^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { aes16[ ees'8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } bes16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ bes2. ~ }
>>
 | \pageBreak 
<<
 { bes4 ~ bes8.[ aes16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes4 ~ aes16[ e'8.^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" }] ~ }
>>
 |  
<<
 { e'1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.1 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 dis'16^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e'1 ~ }
>>
 |  
<<
 { e'1 ~ }
>>
 | \break \noPageBreak 
<<
 { e'1 }
>>
 |  
<<
 { r4 g'8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ c''16^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } bes'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ bes'2 ~ }
>>
 |  
<<
 { bes'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+10"} \hide c8 
 dis16^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 dis'16^\markup { "+12"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
d''16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes'2. ~ bes'16[ r8.] }
>>
 | \break \noPageBreak 
<<
 { r16[ bes8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ bes2. ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 |  
<<
 { bes2. ~ bes8[ ges'8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { ges'4 aes16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ e'16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "8/5" } d'8^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ d'2 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 dis16^\markup { "+12"} \hide c8 
 d16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 d'16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "11/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-39"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 |  
<<
 { d'2. ~ d'8.[ r16] }
>>
 |  
<<
 { r8.[ a'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ a'2 f'4^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 | \break \noPageBreak 
<<
 { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.4 } 
\stopStaff s4. \startStaff \clef bass s16 
d16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "55/32" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d'16^\markup { "-39"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "55/32" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "55/32" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 | \break \noPageBreak 
<<
 { f'2. ~ f'16[ r8.] }
>>
 |  
<<
 { r16[ d'8.^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'8[ e8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e16[ fis16^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } cis'8^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ cis'8.[ e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] }
>>
 |  
<<
 { b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ d'8.^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] e8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ gis8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] cis'2^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { cis'2 e2^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d16^\markup { "-39"} \hide c8 
 e,16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "55/32" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "55/32" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "55/32" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
d''16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e1 ~ }
>>
 |  
<<
 { e2. ~ e8.[ r16] }
>>
 |  
<<
 { r8.[ b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b2. ~ }
>>
 |  
<<
 { b4 ~ b8[ ais8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] gis2^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ }
>>
 | \break \noPageBreak 
<<
 { gis4 ~ gis16[ fis8.^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis2 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 10.6 } 
\stopStaff s4. \startStaff \clef bass s16 
e,16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-39"} \hide c''8 
 e'16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "55/32" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis1 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 |  
<<
 { fis4 ~ fis16[ r8.] r16[ b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } d'8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'16[ e8.^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] }
>>
 | \pageBreak 
<<
 { fis4^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ fis8.[ cis'16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] fis16^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ d'8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e4 ~ }
>>
 |  
<<
 { e8.[ ais16^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais4 ~ ais16[ gis8.^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] b4^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } }
>>
 |  
<<
 { d'8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ cis'8^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ cis'16[ ais8.^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ ais2 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais4 fis2.^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e,16^\markup { "+23"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis1 ~ }
>>
 |  
<<
 { fis1 ~ }
>>
 |  
<<
 { fis4 ~ fis8.[ r16] r8.[ b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b16[ ais8.^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ais4 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ d'8.^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'2 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.2 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+23"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
e'16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 |  
<<
 { d'8.[ r16] r8.[ des'16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ des'4 ~ des'8[ a'8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] }
>>
 | \break \noPageBreak 
<<
 { des'16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ f'8.^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ f'8.[ ees'16^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ ees'2 ~ }
>>
 |  
<<
 { ees'4 des'2.^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { des'2 ~ des'8.[ a'16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ a'4 ~ }
>>
 |  
<<
 { a'1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.3 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-36"} \hide c'8 
 e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e'16^\markup { "+23"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { a'1 ~ }
>>
 |  
<<
 { a'1 ~ }
>>
 |  
<<
 { a'4 ~ a'16[ r8.] r16[ b8.^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ b16[ gis8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } d'16^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] }
>>
 |  
<<
 { ais4^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ e8.^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e16[ fis8.^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ b8.^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] }
>>
 | \break \noPageBreak 
<<
 { d'1^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.4 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "-36"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
e16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'2 ~ d'8[ r8] r8[ ais8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 | \break \noPageBreak 
<<
 { ais2. ~ ais8.[ b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] }
>>
 |  
<<
 { gis4^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ gis16[ e8.^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ e16[ fis8.^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ fis16[ b8.^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { b1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+10"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 e16^\markup { "+23"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "-36"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b1 ~ }
>>
 |  
<<
 { b2. ~ b8[ r8] }
>>
 |  
<<
 { r8[ f'8^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ f'2. ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 \bar "||" \pageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "-36"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "13/10" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 |  
<<
 { f'2. ~ f'8.[ r16] }
>>
 |  
<<
 { r8.[ des'16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ des'8.[ f'16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ f'8.[ ees'16^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ ees'4 ~ }
>>
 | \break \noPageBreak 
<<
 { ees'1 ~ }
>>
 |  
<<
 { ees'1 ~ }
>>
 |  
<<
 { ees'1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 11.7 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "-36"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "13/10" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ees'1 ~ }
>>
 | \break \noPageBreak 
<<
 { ees'8.[ r16] r8.[ ges'16^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ ges'8[ aes8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes4 ~ }
>>
 |  
<<
 { aes16[ d'16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } c'8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ees'4^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ ees'8[ aes8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] c'16^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ ges'16^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } d'8^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] }
>>
 |  
<<
 { bes4^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } f'2.^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 | \break \noPageBreak 
<<
 { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 |  
<<
 { f'2 aes2^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 \bar "|.|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 12.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+10"} \hide c8 
 ais,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
gis16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { aes1 ~ }
>>
 |  
<<
 { aes2. r4 }
>>
 |  
<<
 { c'16^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ ges'8.^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ ges'16[ aes8.^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes8[ ees'8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ ees'4 ~ }
>>
 |  
<<
 { ees'1 ~ }
>>
 | \break \noPageBreak 
<<
 { ees'1 ~ }
>>
 |  
<<
 { ees'1 ~ }
>>
 |  
<<
 { ees'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 12.2 } 
\stopStaff s4. \startStaff \clef bass s16 
ais,16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+10"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-50"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ees'1 ~ }
>>
 | \break \noPageBreak 
<<
 { ees'2 ~ ees'8.[ r16] r8.[ d'16^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'4 ~ d'16[ bes16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } d'8^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'2 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 12.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais,16^\markup { "+14"} \hide c8 
 f,16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-50"} \hide c'8 
 ais16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+10"} \hide c''8 
 f'16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 }
>>
 |  
<<
 { r4 g'2.^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 | \pageBreak 
<<
 { g'16[ bes16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } c'16^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } d'16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ d'2. ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 12.4 } 
\stopStaff s4. \startStaff \clef bass s16 
f,16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 ais16^\markup { "+14"} \hide c'8 
 f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'4 ~ d'8[ r8] r8[ f8^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f8.[ d'16^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ }
>>
 |  
<<
 { d'8[ f8^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f8[ c'8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ c'4 aes4^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ }
>>
 | \break \noPageBreak 
<<
 { aes2. ~ aes8[ bes8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { bes4 ~ bes8.[ d'16^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ d'2 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f,16^\markup { "-50"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
f'16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'4 r4 c'8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ f8^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] aes8.^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ des'16^\markup { \pad-markup #0.2 "-9"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 |  
<<
 { des'8.[ f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f2. ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.2 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f'16^\markup { "-50"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f1 ~ }
>>
 | \break \noPageBreak 
<<
 { f4 ~ f8[ r8] r8[ aes8^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ aes4 ~ }
>>
 |  
<<
 { aes16[ c'8.^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ c'8[ g8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ g16[ c'8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ f8.[ bes16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "+37"} \hide c8 
 g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "10/9" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
f16^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "5/4" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "-46"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "10/9" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes1 ~ }
>>
 | \break \noPageBreak 
<<
 { bes1 ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 |  
<<
 { bes1 ~ }
>>
 |  
<<
 { bes2. ~ bes8[ r8] }
>>
 | \break \noPageBreak 
<<
 { r8[ aes8^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ aes2. ~ }
>>
 |  
<<
 { aes8.[ c'16^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ c'4 g2^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.4 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 f16^\markup { "-50"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "+37"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "10/9" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { g1 ~ }
>>
 |  
<<
 { g8.[ r16] r8.[ fis'16^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ fis'2 ~ }
>>
 | \break \noPageBreak 
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'2. ~ fis'8.[ f'16^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] }
>>
 |  
<<
 { gis2^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } c'16^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }[ dis'8.^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ dis'4 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.5 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
gis16^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "10/9" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { dis'1 ~ }
>>
 | \pageBreak 
<<
 { dis'2 ~ dis'16[ r8.] r16[ gis8.^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 13.6 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "+37"} \hide c'8 
 g16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "10/9" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { gis1 ~ }
>>
 | \break \noPageBreak 
<<
 { gis1 ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 |  
<<
 { gis1 ~ }
>>
 |  
<<
 { gis2. r4 }
>>
 | \break \noPageBreak 
<<
 { dis'2.^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } c'8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }[ g8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { g16[ e'16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } a8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ a8[ d'8^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ d'8[ dis'16^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] g16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }[ ais8.^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 |  
<<
 { ais1 ~ }
>>
 | \break \noPageBreak 
<<
 { ais1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 14.1 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g16^\markup { "-46"} \hide c'8 
 d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { ais1 ~ }
>>
 |  
<<
 { ais4 ~ ais8[ r8] r8[ g'16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ais'16^\markup { \pad-markup #0.2 "-3"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] a'8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ d'8^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { d'4 f'8^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }[ b'8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b'16[ e'16^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } a'8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ a'4 ~ }
>>
 | \break \noPageBreak 
<<
 { a'2 g'4^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } b'8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ f'8^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 14.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g,16^\markup { "-46"} \hide c8 
 a,16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "3/2" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f'2. ~ f'8.[ r16] }
>>
 | \break \noPageBreak 
<<
 { r8.[ d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ d'2. ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 14.3 } 
\stopStaff s4. \startStaff \clef bass s16 
a,16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "-46"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'2. r4 }
>>
 | \break \noPageBreak 
<<
 { g'1^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ }
>>
 |  
<<
 { g'1 ~ }
>>
 |  
<<
 { g'2 ~ g'8[ e'8^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ e'4 ~ }
>>
 |  
<<
 { e'1 ~ }
>>
 | \break \noPageBreak 
<<
 { e'1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 14.4 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 a,16^\markup { "-42"} \hide c8 
 d16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
d''16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { e'1 ~ }
>>
 |  
<<
 { e'1 ~ }
>>
 |  
<<
 { e'4 ~ e'8.[ r16] r8.[ a'16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ a'4 ~ }
>>
 | \pageBreak 
<<
 { a'2. ~ a'8.[ d'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { d'2 ais'2^\markup { \pad-markup #0.2 "-3"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { ais'2 ~ ais'8[ f'16^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } a'16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ a'4 ~ }
>>
 |  
<<
 { a'2. d'4^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 | \break \noPageBreak 
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 |  
<<
 { d'1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.1 } 
\stopStaff s4. \startStaff \clef bass s16 
d16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d'16^\markup { "-44"} \hide c'8 
 a16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "4/3" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-44"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { d'1 ~ }
>>
 | \break \noPageBreak 
<<
 { d'4 ~ d'8.[ r16] r8.[ f'16^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] c'4^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" } ~ }
>>
 |  
<<
 { c'4 ~ c'8.[ fis'16^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] e'2^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { e'16[ a8.^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ a2. ~ }
>>
 |  
<<
 { a2 ~ a8[ f'8^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ f'4 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d16^\markup { "-44"} \hide c8 
 g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "12/11" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
a16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "4/3" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "+7"} \hide c''8 
 d''16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 "16/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f'2. ~ f'8.[ r16] }
>>
 |  
<<
 { r8.[ c'16^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ c'8[ d'8^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ d'2 }
>>
 |  
<<
 { e'1^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 |  
<<
 { e'16[ a16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } fis'8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ fis'4 ~ fis'8.[ c'16^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ c'4 ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.3 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 a16^\markup { "-42"} \hide c'8 
 g16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "12/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 d''16^\markup { "-44"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "8/7" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c'1 ~ }
>>
 |  
<<
 { c'4 r4 g2^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { g4 ~ g8[ d'16^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ees'16^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ ees'8.[ bes16^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ bes4 ~ }
>>
 | \break \noPageBreak 
<<
 { bes16[ f'8.^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ f'2. ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.4 } 
\stopStaff s4. \startStaff \clef bass s16 
g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g16^\markup { "+7"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+25"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f'1 ~ }
>>
 |  
<<
 { f'2. ~ f'16[ r8.] }
>>
 | \break \noPageBreak 
<<
 { r16[ fis'8.^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ fis'4 ~ fis'8.[ b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b4 ~ }
>>
 |  
<<
 { b8[ gis'8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] a'8^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ f'8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ f'16[ b8.^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b4 }
>>
 |  
<<
 { cis'1^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.5 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g,16^\markup { "+7"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
g'16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis'1 ~ }
>>
 | \break \noPageBreak 
<<
 { cis'1 ~ }
>>
 |  
<<
 { cis'1 ~ }
>>
 |  
<<
 { cis'2 ~ cis'8.[ r16] r8.[ fis'16^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { fis'16[ gis'8.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ gis'8[ b8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b8.[ dis'16^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ dis'8.[ cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] }
>>
 | \pageBreak 
<<
 { a'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }[ gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } f'8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ f'2. ~ }
>>
 |  
<<
 { f'8[ gis'8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ gis'2. ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+25"} \hide c8 
 g,16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "+7"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { gis'2. r4 }
>>
 |  
<<
 { fis'1^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } ~ }
>>
 | \break \noPageBreak 
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'2 ~ fis'8.[ dis'16^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ dis'16[ b8.^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.7 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g,16^\markup { "+7"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "14/11" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+25"} \hide c''8 
 g'16^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b1 ~ }
>>
 |  
<<
 { b2 ~ b8.[ r16] r8.[ fis'16^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 \bar ".|" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 15.8 } 
\stopStaff s4. \startStaff \clef bass s16 
b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 g'16^\markup { "+7"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "14/11" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { fis'1 ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'1 ~ }
>>
 |  
<<
 { fis'2. ~ fis'16[ r8.] }
>>
 | \break \noPageBreak 
<<
 { r16[ b8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } a'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ a'8[ fis'8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ fis'16[ f'8.^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] b4^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 |  
<<
 { b4 ~ b16[ gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } dis'16^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ dis'8.[ b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] fis'16^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ f'8.^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 |  
<<
 { f'1 ~ }
>>
 | \break \noPageBreak 
<<
 { f'1 ~ }
>>
 \bar "|.|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.1 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+25"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { f'1 ~ }
>>
 |  
<<
 { f'4 ~ f'16[ r8.] r16[ dis'8.^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ dis'4 ~ }
>>
 |  
<<
 { dis'1 ~ }
>>
 | \break \noPageBreak 
<<
 { dis'2. a'4^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { a'16[ cis'8.^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ cis'16[ b8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ gis'8.[ dis'16^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ dis'4 ~ }
>>
 |  
<<
 { dis'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.2 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "+29"} \hide c8 
 b,16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+25"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { dis'1 ~ }
>>
 | \break \noPageBreak 
<<
 { dis'4 ~ dis'8.[ r16] r8.[ g'16^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ g'16[ gis'8.^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] }
>>
 |  
<<
 { cis'4^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ cis'8[ b'16^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } e'16^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" }] ~ e'4 ais'4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" } ~ }
>>
 |  
<<
 { ais'8[ g'8^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ g'16[ b'8.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b'2 ~ }
>>
 |  
<<
 { b'2 ~ b'16[ gis'8.^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ gis'4 ~ }
>>
 | \pageBreak 
<<
 { gis'1 ~ }
>>
 |  
<<
 { gis'1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.3 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b,16^\markup { "+25"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+29"} \hide c'8 
 b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { gis'1 ~ }
>>
 |  
<<
 { gis'8[ r8] r8[ cis'8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ cis'2 ~ }
>>
 | \break \noPageBreak 
<<
 { cis'1 ~ }
>>
 |  
<<
 { cis'2 ~ cis'8[ dis'8^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ dis'16[ gis'8.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 |  
<<
 { gis'4 ~ gis'8[ b8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ b2 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 | \break \noPageBreak 
<<
 { b1 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.4 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
b16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+25"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b1 ~ }
>>
 |  
<<
 { b1 ~ }
>>
 |  
<<
 { b8[ r8] r8[ dis'8^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ dis'2 ~ }
>>
 | \break \noPageBreak 
<<
 { dis'1 ~ }
>>
 |  
<<
 { dis'2. ~ dis'8[ f'8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ }
>>
 |  
<<
 { f'2 ~ f'8[ cis'8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ cis'4 ~ }
>>
 |  
<<
 { cis'1 ~ }
>>
 \bar "||" \break \noPageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.5 } 
\stopStaff s4. \startStaff \clef bass s16 
cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "9/8" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b16^\markup { "+25"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "16/13" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis'16^\markup { "-35"} \hide c''8 
 b'16^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 "16/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { cis'1 ~ }
>>
 |  
<<
 { cis'16[ r8.] r16[ aes8.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes2 ~ }
>>
 |  
<<
 { aes1 ~ }
>>
 |  
<<
 { aes1 ~ }
>>
 | \break \noPageBreak 
<<
 { aes2. bes4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.6 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "+29"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "-35"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 b'16^\markup { "+25"} \hide c''8 
 cis''16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { bes1 ~ }
>>
 |  
<<
 { bes4 ~ bes8.[ r16] r8.[ g'16^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] b'4^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { b'4 ~ b'8[ gis'8^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ gis'16[ e'16^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ais'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ ais'4 ~ }
>>
 | \break \noPageBreak 
<<
 { ais'8.[ b'16^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] ~ b'2. ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.7 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis,16^\markup { "-35"} \hide c8 
 cis16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+29"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s8. \startStaff \clef treble s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis''16^\markup { "+29"} \hide c''8 
 gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { b'2. ~ b'8.[ r16] }
>>
 |  
<<
 { r8.[ aes16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] des'4^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ des'8[ aes16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } c'16^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ c'4 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 | \break \noPageBreak 
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 \bar "||" \pageBreak 
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.8 } 
\stopStaff s8. \startStaff \clef bass s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis16^\markup { "+29"} \hide c8 
 gis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 gis16^\markup { "-35"} \hide c'8 
 cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { c'1 ~ }
>>
 |  
<<
 { c'1 ~ }
>>
 |  
<<
 { c'2. ~ c'8.[ r16] }
>>
 |  
<<
 { r8.[ gis'16^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] g'4^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" } ~ g'8[ gis'16^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } dis'16^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] cis'4^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" } ~ }
>>
 | \break \noPageBreak 
<<
 { cis'8.[ g'16^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] gis'4^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" } b'2^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" } ~ }
>>
 |  
<<
 { b'4 ~ b'8[ g'8^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ g'2 ~ }
>>
 \bar "||"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.9 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \hide c16 
 \stopStaff s4. \startStaff \clef alto s16 
cis'16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "18/13" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { g'1 }
>>
 |  
<<
 { r4 e'2^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 "6/5" } ~ e'8[ ais'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] }
>>
 | \break \noPageBreak 
<<
 { gis'8^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }[ cis'8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ cis'8[ b'8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 "7/4" }] dis'8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }[ gis'8^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ gis'16[ cis'8.^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 |  
<<
 { cis'4 ~ cis'8[ g'8^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ g'2 ~ }
>>
 |  
<<
 { g'1 ~ }
>>
 \bar ".|"  
<<
\mark \markup { 
\halign #-1 
 \relMark ##{ { 
 \time 15/8 
 \once \override Staff.Clef.stencil = ##f 
 \mark \markup { \bold \override #'(box-padding . 0.5) \box 16.10 } 
\stopStaff s4. \startStaff \clef bass s16 
gis,16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "III"}-\tweak HorizontalBracket.Y-offset #8.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 2) 
 -\tweak HorizontalBracket.edge-height #'(1 . 1) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(3 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \hide c16 
 \stopStaff s8. \startStaff \clef alto s16 
\once \override TextScript.color = #(rgb-color 0.6 0.6 0.6) 
 \tweak Accidental.color #(rgb-color 0.6 0.6 0.6) 
 \tweak NoteHead.color #(rgb-color 0.6 0.6 0.6) 
 cis'16^\markup { "+29"} \hide c'8 
 gis16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "18/13" }^\markup{\large \raise #2 "II"} \stopGroup \hide c'16 
 -\tweak HorizontalBracket.Y-offset #5.5
 -\tweak HorizontalBracket.shorten-pair #'(1 . 3) 
 -\tweak HorizontalBracket.edge-height #'(0 . 0) 
 -\tweak HorizontalBracketText.text \markup { \normalsize \lower #0 \pad-markup #0.2 "1/1" } \startGroup 
 \stopStaff s4. \startStaff \clef treble s16 
gis'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }^\markup{\large \raise #2 "I"} \stopGroup \stopGroup 
 \hide c''16 
 }#}} { g'1 ~ }
>>
 | \break \noPageBreak 
<<
 { g'1 ~ }
>>
 |  
<<
 { g'1 ~ }
>>
 |  
<<
 { g'2 r4 bes4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" } ~ }
>>
 |  
<<
 { bes2 ~ bes16[ ees'8.^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 "3/2" }] ~ ees'8.[ aes16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ }
>>
 | \break \noPageBreak 
<<
 { aes8.[ c'16^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 "5/4" }] ~ c'4 ~ c'16[ aes8.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 "1/1" }] ~ aes8[ e'8^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 "13/8" }] ~ }
>>
 |  
<<
 { e'16[ bes8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 "9/8" }] ~ bes2. ~ }
>>
 |  
<<
 { bes4 ~ bes8[ des'8^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 "11/8" }] ~ des'2 ~ }
>>
 |  
<<
 { des'1 ~ }
>>
 | \break \noPageBreak 
<<
 { des'1 ~ }
>>
 |  
<<
 { des'1 ~ }
>>
 |  
<<
 { des'1 ~ }
>>
 |  
<<
 { des'1 \fermata  }>> \bar "|." 
} 

>>
>>